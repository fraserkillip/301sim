package com.fraserkillip.java.sim301;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Michael McKnight on 29/07/14.
 */
@NoArgsConstructor
@AllArgsConstructor
public class Direction {

    @Getter private int xDir, yDir;
    
    public void addDirection(Dir dir) {
        this.xDir += dir.xDir;
        this.yDir += dir.xDir;
        
        if (Math.abs(this.xDir) > 1) {
            this.xDir -= dir.xDir;
        }
        if (Math.abs(this.yDir) > 1) {
            this.xDir -= dir.yDir;
        }
    }

    @AllArgsConstructor
    public enum Dir {
        NORTH(0, 1), SOUTH(0, -1), EAST(1, 0), WEST(-1, 0),
        NORTH_EAST(1, 1), SOUTH_EAST(-1, 1),
        NORTH_WEST(1, -1), SOUTH_WEST(-1, -1);
        
        private final int xDir, yDir;
    }
}
