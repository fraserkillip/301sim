package com.fraserkillip.java.sim301;

import com.fraserkillip.java.sim301.common.ITickable;
import com.fraserkillip.java.sim301.graphics.IRenderable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fraserkillip on 27/07/14.
 */
public class Chassis implements IRenderable, ITickable, KeyListener, MouseListener {

    private int x, y, width, height, rotation, speed;

    private List<PhotoTransistor> transistorList = new ArrayList<PhotoTransistor>();

    public Chassis(int x, int y, int width, int height, int rotation, int speed) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.rotation = rotation;
        this.speed = speed;
    }

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        g.setColor(Color.blue);
        g.drawRect(x, y, width, height);

        for(PhotoTransistor pt : transistorList) {
            pt.draw(g);
        }

        GraphicsHelper.popState(g);
    }

    @Override
    public void tick(float delta) {
        for(PhotoTransistor pt : transistorList) {
            pt.tick(delta);
        }
    }

    public List<PhotoTransistor> getTransistorList() {
        return transistorList;
    }

    public void addPhotoTransistor(PhotoTransistor pt) {
        transistorList.add(pt);
    }

    public void removePhotoTransistor(PhotoTransistor pt) {
        transistorList.remove(pt);
    }

    @Override
    public void keyPressed(int i, char c) {
        System.out.println(i);
    }

    @Override
    public void keyReleased(int i, char c) {

    }

    @Override
    public void mouseWheelMoved(int i) {

    }

    @Override
    public void mouseClicked(int i, int i2, int i3, int i4) {

    }

    @Override
    public void mousePressed(int i, int i2, int i3) {

    }

    @Override
    public void mouseReleased(int i, int i2, int i3) {
        System.out.println("Mouse clicked");
    }

    @Override
    public void mouseMoved(int i, int i2, int i3, int i4) {

    }

    @Override
    public void mouseDragged(int i, int i2, int i3, int i4) {

    }

    @Override
    public void setInput(Input input) {

    }

    @Override
    public boolean isAcceptingInput() {
        return false;
    }

    @Override
    public void inputEnded() {

    }

    @Override
    public void inputStarted() {

    }
}
