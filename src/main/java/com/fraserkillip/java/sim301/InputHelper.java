package com.fraserkillip.java.sim301;

import org.lwjgl.input.Mouse;

/**
 * Created by fraserkillip on 28/07/14.
 */
public class InputHelper {

    public static boolean isMouseInBounds(int x, int y, int width, int height) {
        int mX = Mouse.getX();
        int mY = GameInfo.GAME_HEIGHT - Mouse.getY();
        return (mX > x && mX < x + width) && (mY > y && mY < y + height);
    }
}
