package com.fraserkillip.java.sim301;

/**
 * Created by fraserkillip on 24/07/14.
 */
public class GameInfo {
    public static final int GAME_WIDTH = 1024;
    public static final int GAME_HEIGHT = 768;
    public static final String GAME_TITLE = "301 Simulator";
}
